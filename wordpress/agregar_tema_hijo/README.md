Pasos para agregar un tema hijo:

## 1. crear una carpeta en el directorio:  wp_content/themes/. En nombre del direcorio debe ser igual al del padre solo que se le debe agregar "-child".

Ejemplo 

- tema padre : ntbigborder
- carpeta hijo: ntbigborder-child

## 2. añadir un archivo functions.php en la carpeta del tema hijo

## 3. añadir archivo css para tema hijo, para ello se debe editar el functions.php, del paso anterior.

function ntbigborder_child_enqueue_styles() {
            wp_enqueue_style( 'nt-big-border-child-style',
                get_stylesheet_directory_uri() . '/style.css',
                false,
                wp_get_theme()->get('Version')
            );
        }

        add_action(  'wp_enqueue_scripts', 'ntbigborder_child_enqueue_styles' );  


## 4. crear un archivo style.css en el tema hijo

## 5. probar si el tema hijo esta funcionado editando el style.css. Si no esta funcionadno, en el admin de wordpress, en la parte de apariencia/temas, activar el tema hijo.

