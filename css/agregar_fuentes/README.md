Sección describe como agregar fuentes a un sitio de wordpress

pasos

## 1. crear o tener un archivo css desde el cual se va a declarar las fuentes

## 2. hacer la conversión de las fuentes de escritorio a web, se puede usar las paginas:

- https://transfonter.org/ttc-unpack
- https://www.fontsquirrel.com/tools/webfont-generator


## 3. Crear carpeta fonts y añadir fuentes. Con el ejemplo descargado de las paginas del paso anterior. Agregar las fuentes o archivos (woff, woff2 u otro)a una carpeta llamada fonts 
que debe estar en donde esta el style.css

## 4. declarar fuentes en el archivo css. Como en el siguiente ejemplo: 


`@font-face {
            font-family: 'bunday_clean_bold_italicBdIt';
            src: url('fonts/bundayclean-bolditalic-webfont.woff2') format('woff2'),
                 url('fonts/bundayclean-bolditalic-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;
  }
`
##  5. usar fuentes 

`.mi-clase{ 
font-family: 'bunday_clean_bold_italicBdIt';
}
`
